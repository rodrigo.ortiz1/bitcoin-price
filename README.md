**Nombre:** Rodrigo Ariel Ortiz

**Fecha:** 07/12/2021

**Materia:** Infraestructura de Servidores: **TP FINAL**

**Profesor:** Sergio Pernas

**Repositorio:** [https://gitlab.com/rodrigo.ortiz1/bitcoin-price](https://gitlab.com/rodrigo.ortiz1/bitcoin-price)

[TOC]

## Descripcion

Se va a armar un repositorio GIT con una aplicación hecha en HTML y que muestra la cotización actual de Bitcoin en diferentes monedas (sea USD, GBP y EURO).
Se va a desplegar usando nginx como servidor web para correr la aplicación, y nginx va a estar dentro de un contenedor Docker. Al lanzar contenedores tambien se puede establecer una variable de entorno que modificara cierto contenido del codigo html de la APP.
Por lo tanto se describe como instalar Docker y configurar contenedores. Y lograr la sincronizacion con el repositorio de GIT.



## Requisitos

La aplicación y todos sus archivos necesarios estaran en el repositorio de Gitlab

[https://gitlab.com/rodrigo.ortiz1/bitcoin-price](https://gitlab.com/rodrigo.ortiz1/bitcoin-price)

El entorno donde se va a desarrollar el proyecto.

[![](https://i.imgur.com/6EbhheE.png)](https://i.imgur.com/6EbhheE.png)


## 1- Instalación Docker

Instalación y configuración de Docker por repositorio oficial

####  1.1- Paquetes y dependencias
Actualizamos e instalamos certificados para trabajar con https

```bash
  apt update && apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
```
[![](https://i.imgur.com/PrU7Tfq.png)](https://i.imgur.com/PrU7Tfq.png)

#### 1.2- Importar llaves

```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
```
Esto se realiza para agregar el repositorio del software Docker, el oficial, es para tener actualizado el software de primera mano.

[![](https://i.imgur.com/9kKB9V1.png)](https://i.imgur.com/9kKB9V1.png)

#### 1.3- Agregar repositorio

```bash
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
 
apt update
```
[![](https://i.imgur.com/ARAarO7.png)](https://i.imgur.com/ARAarO7.png)


#### 1.4- Instalar Docker

```bash
apt install docker-ce docker-ce-cli containerd.io

```

[![](https://i.imgur.com/aIUX0ou.png)](https://i.imgur.com/aIUX0ou.png)

#### 1.5- Chequeo
Chequeamos si se instalo con

```bash
systemctl status docker
```
[![](https://i.imgur.com/0jBizKQ.png)](https://i.imgur.com/0jBizKQ.png)


------------
## 2.Configuración Gitlab

### 2.1 Armar un repositorio GIT con la aplicación. 
[https://gitlab.com/rodrigo.ortiz1/bitcoin-price/](https://gitlab.com/rodrigo.ortiz1/bitcoin-price/)

[![](https://i.imgur.com/Oorzdof.png)](https://i.imgur.com/Oorzdof.png)
[![](https://i.imgur.com/rZEy1ye.png)](https://i.imgur.com/rZEy1ye.png)
[![](https://i.imgur.com/4GWKd7E.png)](https://i.imgur.com/4GWKd7E.png)

### 2.2 Clonar repositorio (usando llave ssh)
Vamos a necesitar generar un par de llaves para tener la conexión con GIT
```bash
ssh-keygen -t rsa -b 2048 -C "rodrigo.ortiz@istea.com.ar"
```

[![](https://i.imgur.com/MaBys32.png)](https://i.imgur.com/MaBys32.png)

Con el comando cat, vamos a conseguir la llave, la cual la vamos a usar para establecerla en la configuracion de usuario-ssh en Gitlab.

```bash
cat .ssh/id_rsa.pub
```

[![](https://i.imgur.com/CS8vwpL.png)](https://i.imgur.com/CS8vwpL.png)

Listo!

[![](https://i.imgur.com/SFcU3CG.png)](https://i.imgur.com/SFcU3CG.png)

Instalamos GIT, si no lo tenemos
```bash
apt install git
```

Procedemos a clonar en el fichero que queramos

```bash
git clone git@gitlab.com:rodrigo.ortiz1/bitcoin-price.git
```

[![](https://imgur.com/1Tv9uri)](https://imgur.com/1Tv9uri)

Sincronización con el repositorio
Al ser la primera vez y que queramos hacer un commit, debemos configurar usuario con git config

```bash
git config --global user.email "rodrigo.ortiz@istea.com.ar"
git config --global user.name "rodrigo.ortiz1"
```

[![](https://i.imgur.com/1Tv9uri.png)](https://i.imgur.com/1Tv9uri.png)

## 3. Explicación de archivos

[![](https://i.imgur.com/8gmYsh4.png)](https://i.imgur.com/8gmYsh4.png)

### 3.1 Dockerfile

Con este tipo de Dockerfile que mostramos a continuación ya podemos compilar y hacer el deploy de la app en el sv, sin la necesidad de descargar todo el repositorio.


Un Dockerfile es un script que contiene todos los comandos para construir una imagen
Docker. 



Lo creamos dentro del directorio donde esta el repositorio (si por alguna razon necesita crearlo)

```bash
vim Dockerfile
```

Con esta configuración

```bash
FROM ubuntu:latest
LABEL version="1"
LABEL description="Bitcoin price via Coindesk  API."
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install nginx -y
ADD https://gitlab.com/rodrigo.ortiz1/bitcoin-price/-/raw/main/index.html /var/www/html
RUN chmod +r /var/www/* -R
ADD https://gitlab.com/rodrigo.ortiz1/bitcoin-price/-/raw/main/default /etc/nginx/sites-available
ADD https://gitlab.com/rodrigo.ortiz1/bitcoin-price/-/raw/main/entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENV SITENAME btcprice
EXPOSE 80 443
ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

```
La instruccion **FROM** indica una imagen base para crear una nueva imagen.

La instruccion **LABEL** es para pasar información acerca de la imagen a crear.

La instruccion **ARG** setea variables de entorno dentro del contenedor que se creara para crear la imagen, y la variable 'DEBIAN_FRONTEND' le pasa el tipo de ejecucion a los programas que lo soliciten.

La  instrucción **ADD** obtiene un fichero de una URL y lo mete dentro de la imagen.

Para tener el directorio /var/www/html, y eso no va a suceder hasta que tengamos instalado el servicio web, por lo tanto, necesitamos la instrucción **RUN** apt update && apt install nginx -y

La instrucción **RUN** ejecuta un comando dentro del contenedor temporal.

RUN chmod +x  otorga permiso de ejecución.
RUN chmod +r  otorga permiso de lectura, para evitar posibles errores al entrar a nuestra app y que nos aparezca por ejemplo "forbidden".

**EXPOSE** es para exponer los puertos del contenedor.

**ENV:** Crea la variable de entorno que se usa para dar el nombre a la aplicación.

**ENTRYPOINT**: En donde decimos que cuando se lanza el contenedor, se va a ejecutar el fichero entrypoint indicado y el fichero entrypoint.sh finaliza con 'exec "$@"' por que a ese fichero se la van a pasar las intrucciones que vengan de 'CMD'

La instruccion **CMD** indica que comando se ejecutara cuando el contenedor se inicie.


[![](https://i.imgur.com/xvyFQai.png)](https://i.imgur.com/xvyFQai.png)

------------

### 3.2 default

Este fichero contiene la configuración del servidor nginx.

Lo creamos dentro del directorio donde esta el repositorio (si por alguna razon necesita crearlo)

```bash
vim default
```
Con esta configuración

```bash
server {
        listen 80;                        # Puerto de escucha del servidor.
        root /var/www/html;     # Directorio root de la aplicación.
        index index.html;           # Archivo index de la aplicación.
        server_name _;               # Nombre por default del servidor.
        location / {
                try_files $uri $uri/ =404;
        }
}

```
[![](https://i.imgur.com/YN5Iur5.png)](https://i.imgur.com/YN5Iur5.png)


------------



### 3.3 entrypoint.sh

Lo creamos dentro del directorio donde esta el repositorio (si por alguna razon necesita crearlo)

```bash
vim entrypoint.sh
```

Con dicha configuración

```bash
#!/bin/bash
#si el script produce algun error,  es para que se detenga su ejecucion
set -e

sed -i 's/SITENAME/'"${SITENAME}"'/' /var/www/html/index.html

#Estamos indicando que cuando encuentre "SITENAME" en nuestro codigo html
# lo reemplace por el contenido de la variable de entorno que hayamos puesto  
# al lanzar el contenedor.

# '$@' representa argumentos pasados al script, estos argumentos son otros scripts.

exec "$@"

```

[![](https://i.imgur.com/r7oLGTu.png)](https://i.imgur.com/r7oLGTu.png)
------------

### 3.4 index.html

Contiene el codigo html de la aplicacion que va a ser ejecutada.
En este caso la app nos va a mostrar la cotización de Bitcoin al momento de ser ejecutada, en valor USD, GBP y Euro.

La información se obtiene mediante la API de Coindesk.

### 3.5 README.md

Es el archivo actualmente en lectura.

## 4. Despliegue del contenedor

### 4.1 Creación de la imagen

```bash
docker build -t bitcoin-price .
```
**build: **  parametro de docker para crear el contenedor.
**-t **  :   asigna el tag "bitcoin-price" al contenedor.
**".":** Es el path, que indica que el archivo Dockerfile se encuentra en el directorio actual.

[![](https://i.imgur.com/7qfEow4.png)](https://i.imgur.com/7qfEow4.png)

Listar imagen:  

```bash
docker images
```
[![](https://i.imgur.com/eRWsXM4.png)](https://i.imgur.com/eRWsXM4.png)


### 4.2 Lanzar contenedor

[![](https://i.imgur.com/LCOVjAx.png)](https://i.imgur.com/LCOVjAx.png)

```bash
docker run -d -p 8080:80 -e SITENAME="Bitcoin price via Coindesk API" --name bitcoin-price bitcoin-price
```


**'-d' :**Ejecuta los procesos en modo 'daemon'
**'- -name': **Nombre para el contenedor
**'-p' :**Mapeo de puertos.
**'bitcoin-price': **nombre de la imagen a utilizar.
**'-e' : **establece la variable de entorno

```bash
docker ps     #muestra los contenedores corriendo
docker ps -a #muestra todos los contenedores
```
**COMMAND:** El programa que se ejecuta cuando se lanza un contenedor.
**STATUS:** El estado del comando que se ejecuta cuando se lanza el contenedor:
-Exited(0) Indica que el programa termino con un 'Exit Status 0'
  '0' el programa termino correctamente. '!0' el programa termino con errores.
**PORTS:** Los puertos (red) que utiliza el contenedor.


### 4.3 Verificación

Vamos a verificar la aplicación, primero obtenemos nuestra ip con
```bash
ifconfig
```
[![](https://i.imgur.com/V4fYVAw.png)](https://i.imgur.com/V4fYVAw.png)

En mi caso: 192.168.0.164

Desde el navegador entramos a 192.168.0.164:8080 y vemos la aplicación en ejecucion.

Tambien podemos ver como tomo la variable de entorno ""Bitcoin price via Coindesk API""

[![](https://i.imgur.com/qVbIS1h.png)](https://i.imgur.com/qVbIS1h.png)

[![](https://i.imgur.com/aQyb0Fz.png)](https://i.imgur.com/aQyb0Fz.png)


## 5. Fuentes
Codigo html
[https://ssaurel.medium.com/create-a-bitcoin-price-index-watcher-in-html5-f441b1e05cd1](https://ssaurel.medium.com/create-a-bitcoin-price-index-watcher-in-html5-f441b1e05cd1)

Clases dictadas por Sergio Pernas en ISTEA - Infraestructura de Servidores
[https://pad.riseup.net/p/infra-clase-11](https://pad.riseup.net/p/infra-clase-11)
[https://pad.riseup.net/p/infra-clase-12](https://pad.riseup.net/p/infra-clase-12)
[https://pad.riseup.net/p/infra-clase-13](https://pad.riseup.net/p/infra-clase-13)
[https://pad.riseup.net/p/infra-clase-14](https://pad.riseup.net/p/infra-clase-14)

[https://docs.docker.com/](https://docs.docker.com/)
[https://about.gitlab.com/install/](https://about.gitlab.com/install/)
