FROM ubuntu:latest
LABEL version="1"
LABEL description="Bitcoin price via Coindesk  API."
ARG DEBIAN_FRONTEND=noninteractive 
RUN apt update && apt install nginx -y
ADD https://gitlab.com/rodrigo.ortiz1/bitcoin-price/-/raw/main/index.html /var/www/html
RUN chmod +r /var/www/* -R
ADD https://gitlab.com/rodrigo.ortiz1/bitcoin-price/-/raw/main/default /etc/nginx/sites-available
ADD https://gitlab.com/rodrigo.ortiz1/bitcoin-price/-/raw/main/entrypoint.sh /
RUN chmod +x /entrypoint.sh 
ENV SITENAME btcprice 
EXPOSE 80 443
ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

